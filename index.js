// iimport the express for starting the API aplication
import express from "express";

// import the dotenv as we can hide the URL monggodb Atlas
import dotenv from "dotenv";


// import the mongoose module
import mongoose from "mongoose";

// import the rouetes form every respective files router
import authRoute from "./routes/authRoutes.js";
import usersRoute from "./routes/usersRoutes.js";
import hotelsRoute from "./routes/hotelsRoutes.js";
import roomsRoute from "./routes/roomsRoutes.js";

const app = express();

// for the hiding mongoAtlas
dotenv.config();

// mongoose connect 
// used by asynchronous is to define if the env or the backend its not connected into the server API 
// it wiil catch the erorr used by  the key method of (throw) to get the error  
// if no APi request let return error
// this initial connection database and API

// let db = mongoose.connection;
// db.on("error", console.error.bind(console, "Connection error"));
// db.once("open", () => console.log("Successfully connected to MongoDB"));


const connect = async () => {
    try {
        await mongoose.connect(process.env.MONGO);
        console.log('Connected to mongoDB.')
      } catch (error) {
        throw (error);
      }
    
}


/**
 this mongoose connection is determine if ever your mongoDb is not network acces within  the ip address 
  0.0.0.0/0  (includes your current IP address) 
 **/ 

mongoose.connection.on("connected", () =>{
   console.log("mongoDB conected!") //-> if the network is access in mongDb connection (active)
})

mongoose.connection.on("disconnected", () =>{
  console.log("mongoDB disconected!") // --> if  not conneted in the acces in the mongoDB connection(is not active)
})

// how API works

app.get("/", (req,res) =>{
  res.send("helloe first request");

})




// Create middleware for an API 
//middleware is most important since it is able to handle the request and response 



  // json() is to handle or to parse the json object from API request body
        app.use(express.json());

        app.use("/api/auth",authRoute);
        app.use("/api/users",usersRoute);
        app.use("/api/hotels",hotelsRoute);
        app.use("/api/rooms",roomsRoute);


  /// thiis middle is only error handler 
  //use the 500 as difine the error outcomes
  // we customize the error handler or most effecient to notice the problem API 
  // craete the variable that contain of error
  //errorStatus -> is define with status has 500 or err.status used by the or operator
  // errorMessage --> i contain of error message or string datatype "something went wrong"
  // return it with the respones or res.status(errorStatus) with json object 
  // json object contain of variety  object error
app.use((err, req,res,next) => {
  // console.log("Hi middleware error");
  // return res.status(500).json("hello error from handler!") 
      const errorStatus = err.status || 500
      const errorMessage = err.message || "Something went wrong"
      return res.status(errorStatus).json({
        success:false,
        status:errorStatus,
        message:errorMessage,
        stack: err.stack,

  });

});



// the port of the backend used by 4000 port as the standard
app.listen(4000, ()=>{
    connect();
    console.log('Connected to backend');

 
})