// import the express API 
import express from "express";

// router 
const router = express.Router();

// always be export the routes in every file
//export the auth unto the index.js file to put as the middleware

export default router
// module.exports = router;