// import the express API 
import express from "express";

// router 
const router = express.Router();
router.get("/", (req,res) => {
    res.send("Helloe, this is Auth endpoint")

})

// always be export the routes in every file
//export the auth unto the index.js file to put as the middleware

// module.exports = router;
export default router