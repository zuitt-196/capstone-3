// import the express API 
import express from "express";

// import the hotel schema from model that will be used for collection of data
import Hotel from "../models/Hotel.js";

// router 
const router = express.Router();

// router.post('/', verify, verifyAdmin, courseControllers.addCourse);

////////// ----->CREATE<-----///////
    // by used of async since we  are now connect the db and create a collection 
    // used with the try and catch it to detemine the exactly error 
    // craete an req.body from postman in order to asure it has work properly
    // if have something problem used asynchronous method 
    // (try) key to define it is sucessfully while the error took up the error with method of (catch)
    router.post("/", async (req, res) => {
        const newHotel = new Hotel(req.body);
        try{
            const saveHotel = await newHotel.save();
            res.status(200).json(saveHotel)
            //200 is to detemine the suceesful
        }catch(err){
            res.status(500).json(err); 
            // 500 is to detemine the error
        }
    })

    ////////// ----->End the CREATE<-----///////



////////---->UPDATE<-----/////
// by the creating the request update
// used the PUT method to update the certain data from database  
// used method of async (asynchronous) is to define the error and sucess with used of key try and catch 
// with the method of POST ,, must be add argumernt in the parameter with :id which mains is geting the id with the specific unique id from data
// to find the id , should be declare findByIdAndUpdate() as the built is express js ,
// findByIdAndUpdate(req.params.id) along the the $set from mongoDb of req.body
router.put("/:id", async (req, res) => {
    
    try{
        const updatedHotel = await Hotel.findByIdAndUpdate(req.params.id, { $set: req.body},{new:true})
        res.status(200).json(updatedHotel);
        //200 is to detemine the suceesfull
    }catch(err){
        res.status(500).json(err); 
        // 500 is to detemine the error
    }
});
////////---->End UPDATE<-----/////






//DELETE
 /// removing the data from database
router.delete("/:id", async (req, res) => {

    try{
        await Hotel.findByIdAndUpdate(req.params.id)
        res.status(200).json("Hotel has been Deleted");
        //200 is to detemine the suceesfull
    }catch(err){
        res.status(500).json(err); 
        // 500 is to detemine the error
    }
});




//GET
// with the used of GET method 
// instead use of findByIdAndUpdate(), getting the specifid data and undate it.
// we able to use findById() getting the specific data without uspdating the information  or data
router.get("/:id", async (req, res) => {

    try{
      const hotel =  await Hotel.findById(req.params.id)
        res.status(200).json(hotel);
        //200 is to detemine the suceesfull
    }catch(err){
        res.status(500).json(err); 
        // 500 is to detemine the error
    }
});




//GET ALL 
// used to GET method 
// used the mongodb method find() to get all data or information
// without the id parameter 
// next() is the other route of middleware
// c 
router.get("/", async (req, res, next) => {
    // console.log("hi am a hotel route")
    // const failed = true
    // const err = new Error()
    //     err.status = 404;
    //     err.message ="sorry not found!"
    //     if(failed) return next(err);    

    try{
      const hotel =  await Hotel.find();
        res.status(200).json(hotel);
        //200 is to detemine the suceesfull
    }catch(err){
        // res.status(500).json(err); 
        // 500 is to detemine the error
        // with the use of middlware in the index file , now we can able to put the next() since it already put the handler or customize  if ever have some error.
        next(err)
    }
});




// always be export the routes in every file
//export the auth unto the index.js file to put as the middleware
export default router
// module.exports = router;