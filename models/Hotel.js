import mongoose from 'mongoose';


const HotelSchema = new mongoose.Schema({
    name:{
        type:String,
        required:[true, "Name is required"]
    },
    type:{
        type:String,
        required:[true, "Type is required"]
    },
    city:{
        type:String,
        required:[true, "City is required"]
    },
    address:{
        type:String,
        required:[true, "Adress is required"]
    },
    distance:{
        type:String,
        required:[true, "Distance is required"]
    },
    photos:{
        type:[String],
    },
    description:{
        type:String,
        required:[true, "Description is required"]
    },
    rating:{
        type:Number,
        min:0,
        max:5
    },
    rooms:{
        type:[String],

    },
    cheapestPrice:{
        type:Number,
        required:[true, "cheapestPrice"]

    },
    featured:{
        type:Boolean,
        default:false
    }


})

// export model Hotel 
export default mongoose.model("Hotel", HotelSchema);